/* 
 * MongoDB management module.
 * 
 * @author: Daniele Gazzzelloni
 */

 module.exports = function(connection){

	// IMPORTS AND CONFIGURATION
	// ==============================================
	var config          = require("./config");
	var autoincrement   = require("mongoose-auto-increment");
	var logger          = require("./logger");
	var moment      	= require('moment');
	var _               = require("underscore");

	//Autoincrement addon for databse
	autoincrement.initialize(connection);

	var models          = require('./models'); // must be AFTER autoincrement is initialized



	function contains (arr, val) {
		for (var x=0; x<arr.length; x++) {
			if (arr[x].toString() == val.toString() ) {
				return true;
			}
		}
		return false;
	}

	function removeDuplicates(p) {
		var uniques     = [];
		var persons     = [];

		for (var x=0; x<p.length; x++) {
			if (!contains(uniques, p[x]._id)) {
				uniques.push(p[x]._id);
				persons.push(p[x]);
			}
		}
		return persons;
	}

	function getPopularMen(eventSize, callback) {
		logger.log("function", "getPopularMen");

		var popularMen = [];

		models.Person.find({gender: config.gender_male})
		.limit(eventSize*2)  // find extra matches to allow some randomness
		.sort('-matchesLength')
		.exec(function (error, data) {
			if (error) {
				logger.log("*", "Unexpected error occured: " + error);
			} 
			else {
                //Now we have event*2 of the most popular men
			     //randomize and return
			     var randomIndexes = pickXRandomNumbersInRange(eventSize, data.length);
			     for (var j = 0; j < randomIndexes.length; j++){
			     	popularMen.push(data[randomIndexes[j]]);
			     }
			 }
			 callback(popularMen);
			});
	}

	function pickXRandomNumbersInRange(x, rangeEnd){
		logger.log("function", "pickXRandomNumbersInRange");

		var arr = [];

		if (x > (rangeEnd)){
			throw "Error: the amount of numbers you need cannot be less than the range";
		}
        // if the number of required numbers is same as range
        else if (x === (rangeEnd)){
        	for (var j = 0; j < rangeEnd; j++){
        		arr.push(j);
        	}
        	arr = shuffle(arr);
        }
        else {
        	while(arr.length < x){
                var randomnumber = Math.ceil(Math.random()*rangeEnd-1); //random number between zero and array length (minus 1)
                var found=false;
                for(var i=0;i<arr.length;i++){
                	if(arr[i]==randomnumber){
                		found=true;
                		break;
                	}
                }
                if (!found){
                	arr[arr.length]=randomnumber;
                }
            }
        }

        console.log(arr);
        return arr;

        // shuffle array
        function shuffle(array) {
        	var counter = array.length, temp, index;

            // While there are elements in the array
            while (counter > 0) {
            // Pick a random index
            index = Math.floor(Math.random() * counter);

            // Decrease counter by 1
            counter--;

            // And swap the last element with it
            temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }
        return array;
    }
}
function getPopularWomen(man, popularMen, popularWomen, eventSize, callback) {
	logger.log("function", "getPopularWomen");

	console.log("popularMen");
	console.log(popularMen);
	if (man < popularMen.length) {
	   //models.Person.find({gender: config.gender_female, matches: popularMen[man]._id})
	   models.Person.find({gender: config.gender_female, matches: popularMen[man]._id})
	   .exec(function (error, data) {
	   	if (error) {
	   		logger.log("*", "Unexpected error occured: " + error);
	   		callback([]);
	   	}
	   	else if (man < popularMen.length) {
	   		getPopularWomen(man+1, popularMen, popularWomen.concat(data), eventSize, callback);
	   	} 
	   	else {
	   		callback(popularWomen);
	   	}
	   });
	}  
	else {
		callback(popularWomen);
	}
}

function getMatches(pid, yob, soulmateYobStart, soulmateYobEnd, gender, frumtype, persons) {
	var matches = [];
	var oppositeSex = (gender === config.gender_male ? config.gender_female : config.gender_male);

	for (var person = 0; person < persons.length; person++) {
		if (matches.indexOf(persons[person]._id) > -1) {
			continue;
		}
		for (var pet = 0; pet < frumtype.length; pet++) {
			if (persons[person].frumtype.indexOf(frumtype[pet].toLowerCase()) > -1 && persons[person].gender == oppositeSex && (yob >= persons[person].soulmateYobStart && yob <= persons[person].soulmateYobEnd) && (persons[person].yob >= soulmateYobStart && persons[person].yob <= soulmateYobEnd)) {
				matches.push(persons[person]._id);
				persons[person].matches.push(pid);
				persons[person].matchesLength += 1;
				savePerson(persons[person], null);
				logger.log(">", "Match found with " + persons[person].firstname + " " + persons[person].lastname + "!");
				break;
			}
		}
	}

	return matches;
}

function personGotMarried(id, callback){
	models.Person.findOne({"_id": id}, function(err, person){
		if (err) console.log(err);
		person.married = true;
		person.save(function(err, result){
			if (err) console.log(err);
			callback(result);
		});
	});
}

function updatePerson(person, callback) {
	console.log("Updating Person... " + person._id);
	models.Person.findOneAndUpdate({_id: person._id}, person, function (err, person) {

		var result = {};
		result.message = "";

		if (err) {
			result.code 	= 400;
			result.message = err.message;
			logger.log("*", result.message);
		} else {
			result.code     =  config.code_OK;
			result.message  = "Person Updated";
		}

		if (callback !== null) {
			callback(result);
		}
	});
}

function savePerson(person, callback) {
	person.save(function (result, person) {
		var message = "";

		if (result) {
			message = result.message;
			logger.log("*", message);
		} else {
			message = config.messages_OK;
		}

		if (callback !== null) {
			callback(person, message);
		}
	});
}


function findAndUpdate(x, pid, data, callback) {
	if (x<data.length) {
		data[x].matches.splice(pid, 1);
		data[x].matchesLength -= 1;
		updatePerson(data[x], function(message) {
			findAndUpdate(x+1, pid, data, callback);
		});
	} else {
		callback(config.messages_OK);
	}
}

var initData = function (callback) {

		// call the callback
		callback();
	};

	var getPersons = function (callback) {
		models.Person.find({}, function (error, data) {
			var result = {};
			if (error) {
				result.message = "Unexpected error occured: " + error;
				logger.log("*", result.message);
			} else if (data.length < 1) {
				result.message = config.messages_noElementsFound;
			} else {
				result.message = config.messages_OK;
				result.persons = data;
			}

			callback(result);
		});
	};
	var getPerson = function (personId, callback) {
		models.Person.findOne({"_id": personId})
		.populate({
			path: 'matches',
			select: 'firstname lastname'
		})
		.exec(function(error, data){
			if (error){
				logger.log("Error", error)
				callback(data)
			} 
			else if (data.length < 1){
				callback("No person found");
			}
			else{
				callback(data);
			}

		});
	}
	var getNames = function(people, callback){
		//Given an array IDs, give me just the names of these people
		models.Person.find({'_id': { $in: people}}, {firstname: 1, lastname: 1}, function(error, data){
			callback(data);
		});
	};

	var getEvents = function (callback) {
		models.Event.find({}, function (error, data) {
			var result = {};
			if (error) {
				result.message = "Unexpected error occured: " + error;
				logger.log("*", result.message);
			} else if (data.length < 1) {
				result.message = config.messages_noElementsFound;
			} else {
				result.message = config.messages_OK;
				result.events = data;
			}

			callback(result);
		});
	};
	var getEvent = function(eventId, callback){
		models.Event.findOne({"eventId": eventId})
		.populate({
			path: 'men',
			model: 'Person',
			select: 'firstname lastname email'
		})
		.populate({
			path: 'women',
			model: 'Person',
			select: 'firstname lastname email'
		})
		.exec(function(err, result){
			if (err) callback(err);
			callback(result)
		})


	}
	var generateGuest = function(eventId, gender, callback){
		getEvent(eventId, function(event){
			var arrayOfFemaleAttendees  = event.women.map(function(item){
				return item["_id"];
			});
			var arrayOfMenAttendees     = event.men.map(function(item){
				return item["_id"];
			});

			if (gender === 'male'){
				models.Person.find({"_id": {"$nin": arrayOfMenAttendees}, "gender": 'male', 'matches': {'$in': arrayOfFemaleAttendees}}).exec(function(err, generatedMen){
					if (err){
						console.log(err);
					}
					else {
						console.log("Number of generated men: " + generatedMen.length);
					//randomly pick one
					var randomnumber = Math.ceil(Math.random()*generatedMen.length-1);
					//add this event to this person
					console.log("randomnumber: " + randomnumber);
					var generatedMan = generatedMen[randomnumber];

					//Find this man and push to array
					models.Person.update({"_id": generatedMan._id}, {"$push": {"eventsAttended": event._id}}, function(err, updateResult1){
						models.Event.update({"_id": event._id}, {"$push": {"men": generatedMan._id}, "$inc": {"size": 1}}, function(err, updateResult2){
							callback(generatedMan); 
						});
					});
				}
			});
			}

			else if (gender === 'female'){
				models.Person.find({"_id": {"$nin": arrayOfFemaleAttendees}, "gender": 'female', 'matches': {'$in': arrayOfMenAttendees}}).exec(function(err, generatedWomen){
					if (err){
						console.log(err);   
					}
					else {
						console.log("Number of generated men: " + generatedWomen.length);
						//randomly pick one
						var randomnumber = Math.ceil(Math.random()*generatedWomen.length-1);
						//add this event to this person
						console.log("randomnumber: " + randomnumber);
						var generatedWoman = generatedWomen[randomnumber];

						//Find this man and push to array
						models.Person.update({"_id": generatedWoman._id}, {"$push": {"eventsAttended": event._id}}, function(err, updateResult1){
							models.Event.update({"_id": event._id}, {"$push": {"men": generatedWoman._id}, "$inc": {"size": 1}}, function(err, updateResult2){
								callback(generatedWoman); 
							});
						});
					}
				}
				);
			}
			else {
				throw "Not male or female. Error";
			}
		});
}

var createPerson = function (pFirstName, pLastName, pYob, pGender, pEmail, pLocation, pFrumtype, pSoulmateYobStart, pSoulmateYobEnd, pSoulmateFrumtype, callback) {
	var person = new models.Person({
		firstname: pFirstName,
		lastname: pLastName,
		yob: pYob,
		gender: pGender,
		email: pEmail,
		location: pLocation,
		frumtype: pFrumtype,
		soulmateYobStart: pSoulmateYobStart,
		soulmateYobEnd: pSoulmateYobEnd,
		soulmateFrumtype: pSoulmateFrumtype,
		matches: [],
		matchesLength: 0,
		eventsAttended: []
	});

    //if this email already exists, return status 406
    models.Person.findOne({"email": pEmail}, function(err, foundPerson){
    	if (err) callback(err);
    	else if (foundPerson != null){
    		console.log("Email does not yet exist");
    		var result = {
    			code: 406,
    			message: "That email already exists in our database"
    		};
    		callback(result);
    	}
    	else{
    		// Retrieves person matches based on frumtype
    		getPersons(function (result) {
    			savePerson(person, function(person, message){
    				if (result.persons !== undefined && result.persons !== null && result.persons.length > 0) {
    					person.matches = getMatches(person._id, pYob, pSoulmateYobStart, pSoulmateYobEnd, pGender, pFrumtype, result.persons);
    					person.matchesLength = person.matches.length;
    				}
    				updatePerson(person, callback);
    			});
    			console.log("Person create called: " + moment().format('MMMM Do YYYY, h:mm:ss a'));
    		});
    	}
    });
};

var deletePerson = function (pObjId, callback) {
	models.Person.remove({_id: pObjId}, function (error, data) {
		var message = "";
		if (error || data.result.ok !== 1) {
			message = "Unexpected error occured: " + error;
			logger.log("*", message);
			callback(message);
		} else if (data.result.n < 1) {
			message = config.messages_noElementsFound;
			callback(message);
		} else {
			// Get all the persons
			models.Person.find({matches: pObjId}, function(error, data){
				// and now update them deleting pObjId
				findAndUpdate(0, pObjId, data, callback); 
			});
		}
	});
};

function saveEvent(event, callback) {
	event.save(function (result, e) {
		var message = "";

		if (result) {
			message = result.message;
			logger.log("*", message);
		} else {
			message = config.messages_OK;
		}

		if (callback !== null) {
			callback(e, message);
		}
	});
}

var createEvent = function (eName, eSize, eLocation, eDate, eMen, eWomen, eNote, callback) {

	var result = {
		message: "",
		men: [],
		women: []
	};
	
	logger.log(eName, eSize, eLocation, eDate, eMen, eWomen, eNote, callback);

		/*
		var event = new models.Event({
			name: eName,
			size: eSize,
			location: eLocation,
			date: eDate,
			men: [],
			women: [],
			note: eNote
		});
*/

var event = new models.Event({
	name: eName,
	size: eSize,
	location: eLocation,
	date: eDate,
	men: [],
	women: [],
	note: eNote
});

//Push men and women to the event list
if (eMen.length >0 && eWomen.length>0) {
	for (var x=0; x<eSize; x++) {
		event.men.push(eMen[x]);
		event.women.push(eWomen[x]);
	}
	//save the event. In the callback, push event to people's eventsAttended lists
	saveEvent(event, function(savedevent, message) {
		console.log("Event Saved!");
		// combine
		var eAll = eMen.concat(eWomen);

		var savedeventId = savedevent._id.toString();
		//Update People's eventsAttended List to add this savedevent
		models.Person.update({"_id": {"$in": eAll}}, {"$push": {"eventsAttended": savedeventId}}, {"multi": true}, function(err, updatedPeople){
			if (err) console.log(err);
			else {
				result.message1          = message;
				result.message2          = updatedPeople;
				result.event            = event;
					//result.updatedPeople    = updatedPeople;
					//console.log("result");
					//console.log(result);
					callback(result);                    
				}

			})
			// eMen[y].eventsAttended.push(event._id);
			// eWomen[y].eventsAttended.push(event._id);
			// updatePerson(eMen[y], null);
			// updatePerson(eWomen[y], null);
		});   
}
};
	/* 
	Created by Raphi
	*/
	var deleteEvent = function(eventId, callback){

	//Remove eventId from anyone who has it
	models.Person.update({"eventsAttended": eventId}, {"eventsAttended": {"$pull": eventId}}, function(err, success){
		if (err) console.log(err);
		else{
			models.Event.remove({"_id": eventId}, function (error, data) {
				var message = "";
				if (error || data.result.ok !== 1) {
					message = "Unexpected error occured: " + error;
					logger.log("*", message);
					callback(message);
				} else {
					var result = {};
					result.status = "200";
					result.message = "Event Deleted";
					callback(result);
				} 
			});
		}

	});
};


var prepareEvent = function (eSize, callback) {
	logger.log("function", "prepareEvent");

	var result = {
		code: -1,
		message: "",
		men: [],
		women: []
	};
	
	var event = new models.Event({
		name: "Preparation",
		size: eSize,
		location: "",
		date: new Date(),
		men: [],
		women: [],
		note: ""
	});

	// Retrieves person matches based on frumtype
	getPopularMen(eSize, function (popularMen) {
		if (popularMen.length < eSize) {
			result.message = config.messages_noMenFound;
			result.code = config.code_notEnoughPeople
			callback(result);
		} else {
			result.men = popularMen;
			getPopularWomen(0, popularMen, [], eSize, function(res) {
				var popularWomen = removeDuplicates(res);
				if (popularWomen.length < eSize) {
					result.message = config.messages_noWomenFound;
					result.code = config.code_notEnoughPeople;
					callback(result);
				} else {
					while (popularWomen.length > popularMen.length) {
						popularWomen.splice(Math.floor((Math.random() * popularWomen.length)), 1);
					}
					result.women = popularWomen;
					result.message = config.messages_OK;
					result.code = config.code_OK;

						/*
						for (var x=0; x<eSize; x++) {
							popularMen[x].eventsAttended.push(event._id);
							popularWomen[x].eventsAttended.push(event._id);
							event.men.push(popularMen[x]);
							event.women.push(popularWomen[x]);
						}
						*/
						callback(result);
					}
				});
		}
	});
};

var removeUserFromEvent = function(eventId, gender, userId, callback){
	console.log("** removeUserFromEvent **");
	var result = {};
		//Remove eventId from User
		models.Person.update({"_id": userId}, {"$pull": {"eventsAttended": eventId}}, function(err, result){
			if (gender === 'male'){
				models.Event.update({"_id": eventId},  {"$pull": {"men": userId}, "$inc": {"size": -1}}, function(err, resultA){
					if (err){
						result.message = err;
						console.log("err");
						console.log(err);
					}
					else {
						result.message = resultA;
						console.log(resultA);
					}
					callback(result);
				});
			}
			else if (gender === 'female'){
				models.Event.update({"_id": eventId},  {"$pull": {"women": userId}, "$inc": {"size": -1}}, function(err, resultB){
					if (err){
						result.message = err;
						console.log("err");
						console.log(err);
					}
					else {
						result.message = resultB;
						console.log(resultB);
					}
					callback(result);
				});
			}
			else {
				throw "Not male or female";
			}
		});
	}
	var addUserToEvent = function(eventId, gender, userId, callback){
		//verify that user exists
		models.Person.findOne({"_id": userId}).exec(function(err, user){
			console.log(user);
			if (err){
				console.log("Error in addUserToEvent");
				console.log(err);
				callback(err);
			}
		//Check if user already has this event
		else if (user.eventsAttended.indexOf(eventId) > -1){
			//Callback and exit
			var result = {};
			result.message = "User is already in this event";
			callback(result)
		}
		else {
			var menOrWomen = (gender === 'male') ? 'men' : 'women';
			if (gender === 'male'){
				models.Event.update({"_id": eventId}, {"$push": {"men": userId}}, unisexcallback);
			}
			else if (gender === 'female'){
				models.Event.update({"_id": eventId}, {"$push": {"women": userId}}, unisexcallback);
			}
			else{
				throw "Not male or female. Error";
			}
		}
	});


		function unisexcallback(err, updatedEvent){
			if (err){
				console.log("Error in addUserToEvent");
				console.log(err);
				callback(err);
			}
			else {
				models.Person.update({"_id": userId}, {"$push": {"eventsAttended": eventId}}, function(err, updatedUser){
					if (err){
						console.log("Error in addUserToEvent");
						console.log(err);
						callback(err);
					}
					else {
						var result = {};
						result.message = "User successfully added to event";
						result.updatedEvent = updatedEvent;
						result.updatedUser = updatedUser;
						callback(result)
					}
				});
			}
		}


	}

	return {
		"connection" : connection,
		"initData" : initData,
		"getPersons" : getPersons,
		"getPerson" : getPerson,
		"getNames" : getNames,
		"createPerson" : createPerson,
		"deletePerson": deletePerson,
		"personGotMarried": personGotMarried,
		"getEvents": getEvents,
		"getEvent": getEvent,
		"prepareEvent": prepareEvent,
		"removeUserFromEvent": removeUserFromEvent,
		"addUserToEvent": addUserToEvent,
		"generateGuest": generateGuest,
		"createEvent": createEvent,
		"deleteEvent": deleteEvent,
		"hidden": {
			"getPopularMen": getPopularMen,
			"pickXRandomNumbersInRange": pickXRandomNumbersInRange
		}
	}
}

