/* 
 * Logger module.
 * 
 * Used for logging to STDOUT/FILE.
 * 
 * @author: Daniele Gazzzelloni
 */

var config      = require('./config');

var log = function(type, message){
    if (config.verboseLogging || type==="*") {
        console.log("%s %s: %s", type, config.appName, message);
    }
};

// utility function: generates a timestamp in the format of 'ddmmyyyyhhmiss'
var generateTimeStamp = function(){
    var d = new Date();
    var data = "";
    data += (d.getDate()<10 ? "0"+d.getDate() : d.getDate()).toString();
    data += ((d.getMonth()+1)<10  ? "0"+(d.getMonth()+1) : d.getMonth()+1).toString();
    data += d.getFullYear().toString().substr(2, 3);
    data += (d.getHours()<10 ? "0"+d.getHours() : d.getHours()).toString();
    data += (d.getMinutes()<10 ? "0"+d.getMinutes() : d.getMinutes()).toString();
    data += (d.getSeconds()<10 ? "0"+d.getSeconds() : d.getSeconds()).toString();

    return data;
};

// format current time to: 'dd-mm-yyyy hh24:mi:ss'
var currentTime = function(){
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth()+1; // zero-index based
    var h = date.getHours();
    var mi = date.getMinutes();
    var s = date.getSeconds();
    
    var ts = (d>9 ? d : "0"+d) + "-" + (m>9 ? m : "0"+m) + "-";
    ts += date.getFullYear() + " " + (h>9 ? h : "0"+h) + ":";
    ts += (mi>9 ? mi : "0"+mi) + ":" + (s>9 ? s : "0"+s);
    return ts;
};

exports.generateTimeStamp       = generateTimeStamp;
exports.log                     = log;
exports.currentTime             = currentTime;