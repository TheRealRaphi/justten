/* 
 * Provides controllers to my test app
 * 
 * @author Daniele Gazzelloni
 */


TestWebApp.controller("formsController", ['$scope', 'db', function ($scope, db) {
    
    $scope.yobs = [];
    for (var x=1920; x<=2012; x++) {
        $scope.yobs.push(x);
    }
    
    $scope.insertPerson = {};
    $scope.deletePerson = {};
    $scope.newEvent     = {};
    
    $scope.insertPerson.submit = function() {
        $scope.insertPerson.log = "";
        $scope.insertPerson.formField1Class = '';
        $scope.insertPerson.formField2Class = '';
        $scope.insertPerson.formField3Class = '';
        $scope.insertPerson.formField4Class = '';
        $scope.insertPerson.formField5Class = '';
        $scope.insertPerson.formField6Class = '';
        $scope.insertPerson.formField7Class = '';
        $scope.insertPerson.formField8Class = '';
        $scope.insertPerson.formField9Class = '';
        $scope.insertPerson.formField10Class = '';
        
        if ($scope.insertPerson.firstname == undefined || $scope.insertPerson.firstname == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField1Class = 'alert-danger';
        } 
        if ($scope.insertPerson.lastname == undefined || $scope.insertPerson.lastname == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField2Class = 'alert-danger';
        }
        if ($scope.insertPerson.email == undefined || $scope.insertPerson.email == 0) {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField3Class = 'alert-danger';
        } 
        if ($scope.insertPerson.age == undefined || $scope.insertPerson.age == 0) {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField4Class = 'alert-danger';
        } 
        if ($scope.insertPerson.gender == undefined || $scope.insertPerson.gender == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField5Class = 'alert-danger';
        } 
        if ($scope.insertPerson.yob == undefined || $scope.insertPerson.yob == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField6Class = 'alert-danger';
        } 
        if ($scope.insertPerson.location == undefined || $scope.insertPerson.location == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField7Class = 'alert-danger';
        }
        if ($scope.insertPerson.pets == undefined || $scope.insertPerson.pets == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField8Class = 'alert-danger';
        }
        if ($scope.insertPerson.soulmateYobStart == undefined || $scope.insertPerson.soulmateYobStart == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField9Class = 'alert-danger';
        } 
        if ($scope.insertPerson.soulmateYobEnd == undefined || $scope.insertPerson.soulmateYobEnd == '') {
            $scope.insertPerson.log = "Form validation check failed.";
            $scope.insertPerson.formField10Class = 'alert-danger';
        } 
        
        // form validation: OK!
        if ($scope.insertPerson.log == "") {
            var firstname       = $scope.insertPerson.firstname;
            var lastname        = $scope.insertPerson.lastname;
            var email           = $scope.insertPerson.email;
            var age             = $scope.insertPerson.age;
            var gender          = $scope.insertPerson.gender;
            var yob             = $scope.insertPerson.yob;
            var location        = $scope.insertPerson.location;
            var soulmateYobStart= $scope.insertPerson.soulmateYobStart;
            var soulmateYobEnd  = $scope.insertPerson.soulmateYobEnd;
            
            var pets            = [];
            var p               = ($scope.insertPerson.pets).split(",");
            for (var x=0; x<p.length; x++) {
                pets.push(p[x].trim());
            }
            
            db.insertPerson(firstname, lastname, email, age, gender, yob, location, pets,
                            soulmateYobStart, soulmateYobEnd).then(function(response){
                $scope.insertPerson.log = "NodeJS response for inserting \""+firstname+" "+lastname+"\" into DB: ";
                $scope.insertPerson.log += response.data.result;
            });
        }
    };
    
    $scope.deletePerson.submit = function() {
        $scope.deletePerson.log = "";
        $scope.deletePerson.errorField = 0;
        $scope.deletePerson.formField1Class = '';
        
        if ($scope.deletePerson.objId == undefined || $scope.deletePerson.objId == '') {
            $scope.deletePerson.log = "Form validation check failed.";
            $scope.deletePerson.formField1Class = 'alert-danger';
        }
        
        // form validation: OK!
        if ($scope.deletePerson.log == "") {
            var objId = $scope.deletePerson.objId;
            
            db.deletePerson(objId).then(function(response){
                $scope.deletePerson.log = "NodeJS response for deleting \""+objId+"\": ";
                $scope.deletePerson.log += response.data.result;
            });
        }
    };
    
    $scope.newEvent.submit = function() {
        $scope.newEvent.log = "";
        $scope.newEvent.eventState = 0;
        $scope.newEvent.errorField = 0;
        $scope.newEvent.formField1Class = '';
        $scope.newEvent.formField2Class = '';
        $scope.newEvent.men = [];
        $scope.newEvent.women = [];
        $scope.newEvent.note = "";
        
        if ($scope.newEvent.name == undefined || $scope.newEvent.name == '') {
            $scope.newEvent.log = "Form validation check failed.";
            $scope.newEvent.formField1Class = 'alert-danger';
        } 
        if ($scope.newEvent.size == undefined || $scope.newEvent.size == 0) {
            $scope.newEvent.log = "Form validation check failed.";
            $scope.newEvent.formField2Class = 'alert-danger';
        } 
        
        // form validation: OK!
        if ($scope.newEvent.log == "") {
            var name    = $scope.newEvent.name;
            var size    = $scope.newEvent.size;
            
            db.prepareEvent(name, size).then( function(response) {
                $scope.newEvent.log = "NodeJS response for event \""+name+" ("+size+")\": ";
                $scope.newEvent.log += response.data.message;
                if (response.data.message == "OK!") {
                    $scope.newEvent.eventState = 1;
                    $scope.newEvent.men = response.data.men;
                    $scope.newEvent.women = response.data.women;
                }
                console.log(response.data);
            });
        }
    };
    
    $scope.newEvent.save = function(eName, eSize, eMen, eWomen, eNote) {
        db.createEvent(eName, eSize, eMen, eWomen, eNote).then( function(response) {
            if (response.data.message == "OK!") {
                $scope.newEvent.eventState = 2;
                console.log("Event \""+eName+"\"saved!");
            } else {
                console.log(response.data);
            }
        });
    };
}]);





TestWebApp.controller("eventsController", ['$scope', 'db', function ($scope, db) {
    $scope.events = [];

    db.getEvents().then( function(response) {
        $scope.events = response.data.events;
    });

    $scope.reloadRoute = function() {
        window.location.reload();
    }
}]);





TestWebApp.controller("personsController", ['$scope', 'db', function ($scope, db) {
    $scope.persons = [];

    db.getPersons().then( function(response) {
        $scope.persons = response.data.persons;
    });

    $scope.reloadRoute = function() {
        window.location.reload();
    }
}]);





TestWebApp.controller("tabController", function ($scope) {
    $scope.tabID = 1;
    $scope.setTab = function(tabID) {
        $scope.tabID = tabID;
    };
    
    $scope.isSet = function(tabID) {
        return ($scope.tabID == tabID);
    };
});
