/* 
 * My AngularJS test app:
 * AngularJS front-end webapp for testing petdating-nodejs module.
 * 
 * @author Daniele Gazzelloni
 */



var TestWebApp = angular.module("TestWebApp", ['ngRoute']);


TestWebApp.config(function ($routeProvider) {
    $routeProvider

            // route for the forms page, same as the home
            .when('/forms', {
                templateUrl: 'views/forms.html',
                controller: 'formsController'
            })

            // route for the events db page
            .when('/events', {
                templateUrl: 'views/events.html',
                controller: 'eventsController'
            })
            
            // route for the events db page
            .when('/persons', {
                templateUrl: 'views/persons.html',
                controller: 'personsController'
            })
            
            .otherwise({
                redirectTo: "/forms"
            });
});