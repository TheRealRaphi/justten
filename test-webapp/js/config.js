/* 
 * Provides editables for test-webapp
 * 
 * @author Daniele Gazzelloni
 */

/******************************************************
*               EDITABLES: API settings 
*******************************************************/

var serverName          = "localhost";
var serverPort          = 46772;

var getPersons_API      = "http://"+serverName+":"+serverPort+"/getPersons";
var getEvents_API       = "http://"+serverName+":"+serverPort+"/getEvents";
var insertPerson_API    = "http://"+serverName+":"+serverPort+"/submitPerson";
var deletePerson_API    = "http://"+serverName+":"+serverPort+"/deletePerson";
var prepareEvent_API     = "http://"+serverName+":"+serverPort+"/prepareEvent";
var createEvent_API     = "http://"+serverName+":"+serverPort+"/createEvent";


/******************************************************
*                   STOP EDITABLES
*******************************************************/