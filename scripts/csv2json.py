import csv
import json
import string
from datetime import datetime

csvfile = open('userdata.csv', 'r')
jsonfile = open('userdata.json', 'w')

fieldnames = ("id", "firstname", "lastname", "gender", "email", "city", 
	"state", "age", "frumtype1", "frumtype2", "frumtype3", "frumtype4", "frumtype5", 
	"soulmateAges", "soulmateFrumness1", "soulmateFrumness2", "soulmateFrumness3", "soulmateFrumness4", "soulmateFrumness5", 
	"comment", "created", "updated", "ip");
reader = csv.DictReader( csvfile, fieldnames)
next(reader)
record = {}
for row in reader:
	record['firstname'] 			= row['firstname']
	record['lastname'] 				= row['lastname']
	record['gender'] 				= row['gender']
	record['email'] 				= row['email']
	record['city'] 					= row['city']
	record['state'] 				= row['state']
	record['yob'] 					= ((datetime.strptime(row['created'], "%m/%d/%Y %H:%M").year) - int(row['age']))
	record['frumtype'] 				= [row['frumtype1'], row['frumtype2'], row['frumtype3'], row['frumtype4'], row['frumtype5']]
	record['frumtype'] 				= [x for x in record['frumtype'] if x is not ''	] ## remove blank columns
	record['soulmateYobEnd'] 		= ((datetime.strptime(row['created'], "%m/%d/%Y %H:%M").year) - int(row['soulmateAges'].split('-')[0]))
	record['soulmateYobStart'] 		= ((datetime.strptime(row['created'], "%m/%d/%Y %H:%M").year) - int(row['soulmateAges'].split('-')[1]))
	record['soulmateFrumtype'] 		= [row['soulmateFrumness1'], row['soulmateFrumness2'], row['soulmateFrumness3'], row['soulmateFrumness4'], row['soulmateFrumness5']]
	record['soulmateFrumtype'] 		= [x for x in record['soulmateFrumtype'] if x is not ''	] ## remove blank columns
	record['comment']				= row['comment']
	record['created']				= row['created']
	record['updated']				= row['updated']
	record['matches']				= []
	record['matchesLength']			= 0
	record['ip']					= row['ip']
	##print (datetime.strptime(row['created'], "%m/%d/%Y %H:%M").year)
	json.dump(record, jsonfile)
	jsonfile.write(',')
	##json.dump(json.loads(json.JSONEncoder(record)), jsonfile, indent=4)
print "something worked"
