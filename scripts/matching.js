var fs = require('fs');


var i, j;

mainLoop();

function mainLoop(){
	var persons = JSON.parse(fs.readFileSync('userdata_out.json', 'utf8'));
	for (i=0; i< persons.length; i++){
			console.log("Matching " + persons[i].firstname + ", " + persons[i].gender +  " with ");

			persons[i].matches 			= getMatches(persons[i]._id, persons[i].yob, persons[i].soulmateYobStart, persons[i].soulmateYobEnd, persons[i].soulmateFrumtype, persons[i].frumtype, persons[i].gender, persons);
			persons[i].matchesLength 	= persons[i].matches.length;
		}
		fs.writeFile('userdate_matched.json', JSON.stringify(persons, null, 4), function(err) {
			if(err) {
				console.log(err);
			} else {
				console.log("JSON saved to test.json");
			}
		}); 
	}

	function getMatches(pid, yob, soulmateYobStart, soulmateYobEnd, soulmateFrumtype, frumtype, gender, persons) {
		var matches = [];
		var oppositeSex = (gender === 'male'? 'female' : 'male');

		for (var j = 0; j < persons.length; j++) {
			//console.log("	against " + persons[j].firstname);
	        /*
	        if (matches.indexOf(persons[j]._id) > -1) {
	            continue;
	        }
	        */
	        for (var frumtypeItem = 0; frumtypeItem < frumtype.length; frumtypeItem++) {
	        	if (persons[j].soulmateFrumtype.indexOf(frumtype[frumtypeItem].toLowerCase()) > -1){
	        		if(persons[j].gender === oppositeSex){
	        			if(yob >= persons[j].soulmateYobStart){
	        				if(yob <= persons[j].soulmateYobEnd){
	        					if(persons[j].yob >= soulmateYobStart){
	        						if(persons[j].yob <= soulmateYobEnd){
	        							console.log("	Arrived at: " + persons[j].firstname + " " + persons[j].lastname + ", " + persons[j].gender);

	        							persons[j].matches.push({"$oid": persons[j]._id.$oid});
	        							persons[j].matchesLength = persons[j].matches.length;
	        							matches.push({"$oid": persons[j]._id.$oid});
					                //persons[person].matchesLength += 1;
					                //savePerson(persons[person], null);
					                //logger.log(">", "Match found with " + persons[person].firstname + " " + persons[person].lastname + "!");
					                break;
					            }
					        }
					    }
					}            		
				}
			} 
		}
	}
	return matches;
}

