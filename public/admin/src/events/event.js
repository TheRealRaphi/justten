var eventApp = angular.module('EventApp', ['ConstantsApp', 'ModalsApp']);

eventApp.controller('eventAppCtrl', eventAppCtrl);
eventApp.factory('getEventSrvc', getEventSrvc);
eventApp.factory('generateNewGuestSrvc', generateNewGuestSrvc);
eventApp.factory('removeUserFromEventSrvc', removeUserFromEventSrvc);
eventApp.factory('manuallyAddUserToEventSrvc', manuallyAddUserToEventSrvc);


//====================
// FUNCTIONS
//====================

function eventAppCtrl($scope, modalsFactory, getEventSrvc, generateNewGuestSrvc, removeUserFromEventSrvc, manuallyAddUserToEventSrvc, eventId){
	$scope.status 	= {};
	$scope.status.loading	= false;
	
	$scope.event;
	$scope.eventId = eventId;

	$scope.generateNewGuest 	= generateNewGuest;
	$scope.removeUserFromEvent 	= removeUserFromEvent;
	$scope.manuallyInputPerson 	= manuallyInputPerson;
	$scope.getEmailAddresses 	= getEmailAddresses;

	init();

	//FUNCTIONS
	function init(){
		$scope.status.loading = true;
		getEvent(true);
	}
	function generateNewGuest(gender){
		$scope.status.loading = true;
		generateNewGuestSrvc($scope.event.eventId, gender).execute()
		.success(function(data){
			modalsFactory({
				type: 'info',
				message: "Added " + data.firstname + " " + data.lastname + " to the event"
			});
			getEvent(false);
			console.log(data);
			$scope.status.loading = false;
		})
		.error(function(data){
			console.log(data);
			$scope.status.loading = false;
		});
	}
	function getEvent(shouldCache){
		getEventSrvc(eventId, shouldCache).execute()
		.success(function(data){
			console.log(data);
			$scope.event = data;
			$scope.status.loading = false;
		})
		.error(function(data){
			console.log(data);
			console.log(status);
			if (status === 403){
				LogoutSrvc();
			}
			$scope.status.loading = false;
		});
	}
	function removeUserFromEvent(eventId, gender, userId, firstname, lastname){
		if (confirm("Are you sure you want to remove " + firstname + " " + lastname + " from this event?")){
			$scope.status.loading = false;
			removeUserFromEventSrvc(eventId, gender, userId).execute()
			.success(function(data){
				getEvent(false);
				console.log(data);
				$scope.status.loading = false;			
			})
			.error(function(data, status){
				console.log(data);
				if (status === 403){
					alert("Please login again");
				}
				$scope.status.loading = false;
			});
		}
	}
	function manuallyInputPerson(eventId, gender, userId){
		$scope.status.loading = false;
		manuallyAddUserToEventSrvc(eventId, gender, userId).execute()
		.success(function(data){
			getEvent(false);
			console.log(data);
			modalsFactory({
				type: 'info',
				message: data.message
			});
			$scope.status.loading = false;			
		})
		.error(function(data, status){
			console.log(data);
			if (status === 403){
				alert("Please login again");
			}
			else {
				modalsFactory({
					type: 'info',
					message: 'There was an error. Make sure you have copied in a valid 24 digit user ID'
				});
			}
			$scope.status.loading = false;
		});
	}
	function getEmailAddresses(){
		var email = "";
		for (var m = 0; m < $scope.event.men.length; m++){
			email = email + $scope.event.men[m].email + ", ";
		}
		for (var w = 0; w < $scope.event.women.length; w++){
			email = email + $scope.event.women[w].email + ", ";
		}
		modalsFactory({
			type: 'info',
			message: email
		});
	}
}

function getEventSrvc($http, URL){
	return function(eventId, shouldCache){
		var getEventURL = URL + "/getEvent/" + eventId;
		return {
			execute: function(){
				return $http({
					method: 'GET',
					url: getEventURL,
					cache: shouldCache,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		};
	}
}
function generateNewGuestSrvc($http, URL){
	return function(eventId, gender){
		var generateNewGuestURL = URL + "/event/" + eventId + "/generate/" + gender;
		return {
			execute: function(){
				return $http({
					method: 'GET',
					url: generateNewGuestURL,
					cache: false,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		};
	}
}
function removeUserFromEventSrvc($http, URL){
	return function(eventId, gender, userId){
		var removeUserFromEventURL = URL + "/event/" + eventId + "/user/" + gender + "/" + userId;
		return {
			execute: function(){
				return $http({
					method: 'DELETE',
					url: removeUserFromEventURL,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
};
function manuallyAddUserToEventSrvc($http, URL){
	return function(eventId, gender, userId){
		var addUserToEventURL = URL + "/event/" + eventId + "/user/" + gender + "/" + userId;
		return {
			execute: function(){
				return $http({
					method: 'POST',
					url: addUserToEventURL,
					cache: false,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
};