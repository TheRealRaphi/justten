var eventsApp = angular.module('EventsApp', ['CommonApp', 'ConstantsApp', 'ModalsApp', 'ui.bootstrap', 'smart-table']);

eventsApp.controller('eventsCtrl', [
	'$scope', 
	'$state', 
	'$modal', 
	'$log', 
	'getEventsSrvc', 
	'deleteEventSrvc', 
	'modalEventsSrvc', 
	'LogoutSrvc', 
	'URL', 
	function(
		$scope, 
		$state, 
		$modal, 
		$log, 
		getEventsSrvc, 
		deleteEventSrvc, 
		modalEventSrvc, 
		LogoutSrvc, 
		URL){

		$scope.status 			= {};
		$scope.status.loading 	= false;

		$scope.events = [];
	$scope.displayedEvents = [];//as per stSafeSrc

	$scope.deleteEvent 				= deleteEvent;

	// FUNCTIONS

	$scope.init = function(){
		getEvents(false);
	}

	$scope.makeAlert = function(event){ 	
		var promise = modalEventSrvc(event);
		promise.then(function(data){
			if (data === 'delete'){
				console.log("Deleting Events Temporarily not available");
			}
		});
	} 
	
	function deleteEvent(id){
		if (confirm("Are you sure you want to delete this event?")){
			$scope.status.loading = true;
			deleteEventSrvc(id).execute()
			.success(function(data){
				//reload events
				getEvents(false, function(){
					$scope.status.loading = false;
				});
			})
			.error(function(data){
				$scope.status.loading = false;
			});
		}
	}

	function getEvents(shouldCache, callback){
		getEventsSrvc(shouldCache).execute()
		.success(function(data){
			console.log(data);
			$scope.events = data.events;
			if (callback) callback();
		})
		.error(function(data, status){
			if (status === 403){
				LogoutSrvc();
			}
			if (callback) callback();
		});
	}

	
}]);	


eventsApp.factory('getEventsSrvc', ['$http', 'URL', function($http, URL){
	var getEventsURL = URL + "/getEvents";
	return function(shouldCache){
		return {
			execute: function(){
				return $http({
					method: 'GET',
					url: getEventsURL,
					cache: shouldCache,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
}]);

eventsApp.factory('deleteEventSrvc', ['$http', 'URL', function($http, URL){
	var deleteEventURL = URL + "/deleteEvent";
	return function(id){
		return {
			execute: function(){
				return $http({
					method: 'DELETE',
					url: deleteEventURL,
					data: $.param({id: id}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
}]);
