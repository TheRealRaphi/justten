var CommonApp = angular.module('CommonApp', ['ConstantsApp', 'ngCookies']);


CommonApp.factory('LogoutSrvc', ['$rootScope', '$state', '$cookieStore', 'CookieNames', function($rootScope, $state, $cookieStore, CookieNames){
	return function(){
		$cookieStore.put(CookieNames.LOGGED_IN, false);
		$rootScope.logout();
		//$state.go('login');
	};
}]);
