/**
 * Created by Raphi Stein on 2/10/2015.
 */

 var app = angular.module('AdminApp', [
    'ResourcesApp', 
    'ConstantsApp', 
    'GenerateApp', 
    'PeopleApp', 
    'PersonApp',
    'EventsApp', 
    'EventApp',
    'StatsApp',
    'ModalsApp', 
    'ngCookies', 
    'ui.router', 
    'ui.bootstrap']);

//app.config(["$locationProvider", function($locationProvider) {
//    $locationProvider.html5Mode({
//        enabled: true,
//        requireBase: false
//    });
//}]);
app.config(['$httpProvider', function ($httpProvider) {
            // enable http caching
            $httpProvider.defaults.cache = true;
        }])
app.run(['$state', '$cookieStore', '$rootScope', 'CookieNames', function($state, $cookieStore, $rootScope, CookieNames) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        //$cookies("justten-log", true);
        console.log("toState: " + toState.name );
        var toStateName = toState.name;
        console.log(toStateName.split('.')[0]);

        var stateNamePrefix = toStateName.split('.')[0];
        if (stateNamePrefix === 'protected'){
            var loggedIn = $cookieStore.get(CookieNames.LOGGED_IN);
            if (!loggedIn){
                console.log("Not logged in");
                event.preventDefault(); 
                $state.go('login');
            }
        }
        else if (stateNamePrefix === 'login'){
            var loggedIn = $cookieStore.get(CookieNames.LOGGED_IN);
            if (loggedIn){
                console.log("Already logged in");
                event.preventDefault(); 
                $state.go('protected.admin');
            }
        }
        else {
            console.log("is logged in");
            //dn nothing
        }
    });

}]);
app.run(['$rootScope', 'logoutSrvc', '$state', function($rootScope, logoutSrvc, $state){
    $rootScope.logout = function(){
        console.log("Logging out...");
        logoutSrvc().execute()
        .success(function(data){
            alert("Successfully Logged Out");
            $state.go('login');
        })
        .error(function(data){
            // Report error but do nothing
            alert("Problem Logging out");
        });
    }
}]);


app.config(
    ['$stateProvider', '$urlRouterProvider',
    function ($stateProvider,   $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
        .state("login", {
            url: "/login",
            templateUrl: "/admin/src/login/login.html"
        })
        .state("protected", {
            abstract: true,
            url: '',
            template: '<ui-view />'
        })
        .state("protected.admin", {
            url: "",
            templateUrl: '/admin/src/home/home.html'
        })
        .state("protected.generate", {
            url: "/generate",
            templateUrl: '/admin/src/generate/generate.html',
            controller: 'generateCtrl'
        })
        .state("protected.people", {
            url: "/people",
            templateUrl: '/admin/src/people/people.html',
            controller: 'peopleCtrl'
        })
        .state("protected.stats", {
            url: "/stats",
            templateUrl: '/admin/src/stats.html',
            controller: 'statsAppCtrl'
        })
        .state("protected.person", {
            url: "/people/:personId",
            templateUrl: '/admin/src/people/person.html',
            controller: 'personAppCtrl',
            params: {person: null, personId: null},
            resolve:{
                person: ['$stateParams', function($stateParams){
                    console.log($stateParams);
                    return $stateParams.person;
                }],
                personId: ['$stateParams', function($stateParams){
                    return $stateParams.personId;
                }]
            }
        })
        .state("protected.events", {
            url: "/events",
            templateUrl: '/admin/src/events/events.html',
            controller: 'eventsCtrl'
        })
        .state("protected.event", {
            url: "/events/:eventId",
            templateUrl: '/admin/src/events/event.html',
            controller: 'eventAppCtrl',
            resolve:{
                eventId: ['$stateParams', function($stateParams){
                    return $stateParams.eventId;
                }]
            }
        });
    }
    ]);



app.controller('adminCtrl', ['$rootScope', '$scope', '$cookieStore', '$state', 'LoginSrvc', 'CookieNames', function($rootScope, $scope, $cookieStore, $state, LoginSrvc, CookieNames){
    $scope.status = {};
    $rootScope.loggedIn;

    $scope.login = {};
    $scope.login.username = "";
    $scope.login.password = "";

    $scope.login.adminLogin = adminLogin;


    //FUNCTIONS
    function adminLogin(username, password){
        LoginSrvc(username, password).execute()
        .success(function(data, status){
            alert("Successfully Logged In");
            console.log("Successfully Logged In");
            console.log(data);
            $rootScope.loggedIn = true;
            $cookieStore.put(CookieNames.LOGGED_IN, true);
            $state.go('protected.generate');
        })
        .error(function(data, status){
            $rootScope.loggedIn = false;
            console.log("Error");
            console.log(data);
            if (status === 403){
                $state.go('login');
            }
        });        
    }

}]);

app.factory('logoutSrvc', ['$http', 'URL', function($http, URL){
    var logoutURL = URL + "/adminlogout";
    return function(){
        return {
            execute: function(){
                return $http({
                    method: 'GET',
                    url: logoutURL,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };
    }
}]);