var infoModalApp = angular
    /*
     * ProjectApp (not projects)
     */
    .module('PeopleModalApp', []);


infoModalApp.controller('peopleModalCtrl', ['$scope', '$modalInstance', '$log', 'person',
        function($scope, $modalInstance, $log, person) {

            $scope.person = person;

            $scope.dismiss = function(){
                $modalInstance.close();
            }
        }
    ]
);