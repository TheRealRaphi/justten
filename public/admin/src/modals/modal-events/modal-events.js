var eventsModalApp = angular
    /*
     * ProjectApp (not projects)
     */
     .module('EventsModalApp', ['ConstantsApp']);


     eventsModalApp.controller('eventsModalCtrl', ['$scope', '$modalInstance', '$log', 'eventParticipantsSrvc', 'event', 'person',
        function($scope, $modalInstance, $log, eventParticipantsSrvc, event, person) {

            $scope.event = event;

            $scope.dismiss = function(){
                $modalInstance.close("dismiss");
            }
            $scope.deleteEvent = function(id){
                $modalInstance.close("delete");
            }

            eventParticipantsSrvc(event.men.concat(event.women)).execute()
            .success(function(data, status){
                console.log(data);
                event.participants = data.result;
            })
            .error(function(data, status){
                console.log(data);
            });
        }
        ]
        );

     eventsModalApp.factory('eventParticipantsSrvc', ['$http', 'URL', function($http, URL){
        var getNamesURL = URL + "/getNames";
        return function(people){
            return {
                execute: function(){
                    return $http({
                        method: 'POST',
                        url: getNamesURL,
                        data: $.param({people: people}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    });

                }
            };
        }
    }])