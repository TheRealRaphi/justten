var modalsApp = angular
/*
 * ProjectApp (not projects)
 */
 .module('ModalsApp', ['PeopleModalApp', 'EventsModalApp']);

 modalsApp.factory('modalsFactory', modalsFactory);
 modalsApp.controller('infoModalCtrl', infoModalCtrl);


 infoModalCtrl.$inject = ['$scope', '$modalInstance', 'message'];
 function infoModalCtrl($scope, $modalInstance, message){
    $scope.message = message;

    $scope.dismiss = function(){
        $modalInstance.close("dismiss");
    }
}
modalsApp.factory('modalPeopleSrvc', ['$modal', '$log',
    function($modal, $log) {
        return function (person) {
            var modalInstance = $modal.open({
                templateUrl: "/admin/src/modals/modal-people/modal-people.html",
                controller: 'peopleModalCtrl',
                resolve: {
                    person: function () {
                        return person;
                    }
                }
            });

            modalInstance.result.then(
                function (selectedItem) {
                    $log.info('Promise Kept: Modal dismissed at: ' + new Date());
                },
                function () {
                    $log.info('Promise Not Kept: Modal dismissed at: ' + new Date());
                });

        }
    }
    ]
    );

modalsApp.factory('modalEventsSrvc', ['$modal', '$log', '$q',
    function($modal, $log, $q) {
        return function (event, person) {
            var deferred = $q.defer();

            var modalInstance = $modal.open({
                templateUrl: "/admin/src/modals/modal-events/modal-events.html",
                controller: 'eventsModalCtrl',
                resolve: {
                    person: function () {
                        return person;
                    },
                    event: function(){
                        return event;
                    }
                }
            });

            modalInstance.result.then(
                function (selectedItem) {
                    deferred.resolve(selectedItem);
                }
                );

            return deferred.promise;

        }
    }
    ]
    );


function modalsFactory($modal, $log, $q){
    return function (modalInfo, successCallback, failCallback) {

        /*
        Format:

        {
            tyoe: 'info',
            message: "The message"

        }
        */

        //var deferred = $q.defer();

        console.log(modalInfo);
        switch (modalInfo.type) {
            case 'info':
            var modalInstance = $modal.open({
                templateUrl: "/admin/src/modals/info-modal.html",
                controller: 'infoModalCtrl',
                resolve: {
                    message: function(){
                        return modalInfo.message;
                    }
                }
            });
            modalInstance.result.then(
                function (selectedItem) {
                    if (modalInfo.successCallback)
                        modalInfo.successCallback();
                }
                );
            break;

            default: 
            var modalInstance = $modal.open({
                templateUrl: "modal.html",
                controller: 'modalCtrl',
                resolve: {
                    message: function(){
                        return "Default Modal";
                    }
                }
            });
            break;
        }

        /*
        modalInstance.result.then(
            function (selectedItem) {
                if (successCallback){
                    successCallback();
                }
                else {

                }
                deferred.resolve(selectedItem);
            }
            );
*/

//return deferred.promise;
}
}