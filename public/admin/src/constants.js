var ConstantsApp = angular.module("ConstantsApp", []);

ConstantsApp.constant("URL",
	//"http://localhost:7071");
	"http://justten.herokuapp.com");

ConstantsApp.constant('CookieNames', {
	"LOGGED_IN": 'lin'	
});

ConstantsApp.directive('lineItem', function($compile){
	return{
		template: '<p class="key">{{key}}</p>   <p>{{value || "(None)"}}</p>',
		scope: {
			'key': '=',
			'value': '='
		}
	}
});