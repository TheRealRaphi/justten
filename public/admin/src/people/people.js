
var peopleApp = angular.module('PeopleApp', ['ModalsApp', 'ConstantsApp', 'CommonApp', 'ui.bootstrap', 'smart-table']);

peopleApp.controller('peopleCtrl', ['$rootScope', '$scope', '$state', '$modal', '$log', 'getPeopleSrvc', 'modalPeopleSrvc', 'LogoutSrvc', 'URL', function($rootScope, $scope, $state, $modal, $log, getPeopleSrvc, modalPeopleSrvc, LogoutSrvc, URL){
	$scope.status			= {};
	$scope.status.loading 	= false;
	
	$scope.people = [];
	$scope.displayedPeople = [];//as per stSafeSrc

	$scope.makeAlert = function(person){ 	
		modalPeopleSrvc(person);
	}
	
	init();

	// FUNCTIONS
	function init(){
		$scope.status.loading 	= true;
		getPeopleSrvc().execute()
		.success(function(data, status){
			$rootScope.loggedIn = true;
			console.log(data);
			console.log(status);
			$scope.people = data.persons;
			$scope.displayedPeople = [].concat($scope.people);
			$scope.status.loading 	= false;

		})
		.error(function(data, status){
			$rootScope.loggedIn = false;
			console.log(data);
			console.log(status);
			if (status === 403){
				LogoutSrvc();
			}
			$scope.status.loading 	= false;
		});
	}
}]);


peopleApp.factory('getPeopleSrvc', ['$http', 'URL', function($http, URL){
	var getPeopleURL = URL + "/getPersons";
	return function(){
		return {
			execute: function(){
				return $http({
					method: 'GET',
					url: getPeopleURL,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		};
	}
}]);
