var personApp		= angular.module('PersonApp', ['ConstantsApp']);

personApp.controller('personAppCtrl', ['$rootScope', '$scope', 'getUserSrvc', 'personGotMarriedSrvc', 'personId', personAppCtrl]);
personApp.factory('getUserSrvc', getUserSrvc);
personApp.factory('personGotMarriedSrvc',  personGotMarriedSrvc);


// FUNCTIONS

function personAppCtrl($rootScope, $scope, getUserSrvc, personGotMarriedSrvc, personId){
	$scope.status 			= {};
	$scope.status.loading 	= false;

	$scope.injected = {};
	$scope.injected.personId = personId;

	$scope.personGotMarried = personGotMarried;

	init();

	// CONTROLLER FUNCTONS
	function init(){
		$scope.status.loading 	= true;
		getUser(personId);
	}
	function getUser(personId){
		getUserSrvc(personId).execute()
		.success(function(data){
			$rootScope.loggedIn = true;
			$scope.person = data;
			$scope.status.loading 	= false;
		})
		.error(function(data){
			$rootScope.loggedIn = false;
			$scope.status.loading 	= false;
			console.log(data);
			console.log(status);
			if (status === 403){
				LogoutSrvc();
			}
		});	
	}
	function personGotMarried(id){
		 personGotMarriedSrvc(id).execute()
		.success(function(data){
			alert("Mazal Tov. Marriage Noted");
			getUser(id);
		})
		.error(function(data){
			alert("Marriage not entered. Error");
		});
	}
};

function getUserSrvc($http, URL){
	return function(personId){
		var getPeopleURL = URL + "/getPerson/" + personId;
		return {
			execute: function(){
				return $http({
					method: 'GET',
					url: getPeopleURL,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		};
	}
}

function personGotMarriedSrvc($http, URL){
	return function(userId){
		var personGotMarriedURL = URL + "/" + userId + "/gotMarried";
		return {
			execute: function(){
				return $http({
					method: 'POST',
					url: personGotMarriedURL,
					cache: false,
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
};