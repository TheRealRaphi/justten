/**
 * Created by Raphi Stein on 2/10/2015.
 */

 var generateApp = angular.module('GenerateApp', ['CommonApp', 'ConstantsApp', 'ui.bootstrap']);

 generateApp.controller('generateCtrl', ['$scope', '$state', '$modal', '$log', 'generateGuestListSrvc', 'LogoutSrvc', function($scope, $state, $modal, $log, generateGuestListSrvc, LogoutSrvc){

    $scope.status = {};
    $scope.status.loading = false;

    $scope.menguests = [];
    $scope.womentguests = [];
    $scope.guests = [];

    $scope.guestListSize = 1;
    $scope.generateGuestList = generateGuestList;


    $scope.getCurrentYear = function(){
        return new Date().getFullYear();
    }


    function generateGuestList(){
        $scope.status.loading = true;
        generateGuestListSrvc("Temp Name", $scope.guestListSize).execute()
        .success(function(data){
            console.log(data);
            $scope.menguests = data.men;
            $scope.womenguests = data.women;
            $scope.guests = data.men.concat(data.women)
            $scope.status.loading = false;
        })
        .error(function(data, status){
            console.log(data);
            if (status === 403){
                LogoutSrvc();
            }
            $scope.status.loading = false;
        });
    }

    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: '/admin/src/generate/create-event-modal.html',
            controller: 'generateEventModalCtrl',
            size: size,
            resolve: {
                menguests: function(){
                    return $scope.menguests
                },
                womenguests: function(){
                    return $scope.womenguests
                },
                size: function(){
                    return $scope.guestListSize
                }
            },
            backdrop: false
        });

        modalInstance.result.then(function (abc, def) {
            console.log(abc, def);
            $state.go('protected.events', {}, {reload: true});
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    };
}]);


generateApp.controller('generateEventModalCtrl', ['$scope', '$modalInstance', 'createEventSrvc', 'menguests', 'womenguests', 'size',
    function ($scope, $modalInstance, createEventSrvc, menguests, womenguests, size) {
    $scope.newevent = {};
    $scope.newevent.name = "";
    $scope.newevent.location = "";
    $scope.newevent.date;
    $scope.newevent.note = "";

    $scope.ok = function () {
        //send array of men ID's and ID's
        arrayOfMenIDs = menguests.map(function(item){
            return item._id;
        });
        arrayOfWomenIDs = womenguests.map(function(item){
            return item._id;
        });
        createEventSrvc($scope.newevent.name, size, $scope.newevent.location, $scope.newevent.date, arrayOfMenIDs, arrayOfWomenIDs, $scope.newevent.note).execute()
        .success(function(data){
            console.log(data);
        })
        .error(function(data){
            console.log(data);
        });
        $modalInstance.close("Success");
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };


    // Calendar Widget
    $scope.today = function() {
        $scope.newevent.date = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.newevent.date = null;
    };

    // Disable weekend selection
    //    $scope.disabled = function(date, mode) {
    //        return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    //    };

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
}]);

generateApp.factory('generateGuestListSrvc', ['$http', 'URL', function($http, URL){
    return function(name, size){
        var prepareEventsURL = URL + "/prepareEvent";
        return {
            execute: function(){
                return $http({
                    method: 'POST',
                    url: prepareEventsURL,
                    data: $.param({size: size}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        }
    }
}]);
generateApp.factory('createEventSrvc', ['$http', 'URL', function($http, URL){
    return function(name, size, location, date, men, women, note){
        console.log(name, size, location, date, men, women, note);
        var createEventURL = URL + "/createEvent";
        return {
            execute: function(){
                return $http({
                    method: 'POST',
                    url: createEventURL,
                    data: $.param({name: name, size: size, location: location, date: date, men: men, women: women, note: note}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        }
    }
}]);