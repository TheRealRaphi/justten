var ResourcesApp = angular.module('ResourcesApp', ['ConstantsApp']);

ResourcesApp.factory('LoginSrvc', ['$http', 'URL', function($http, URL){
	return function(username, password){
		var loginURL = URL + "/adminlogin"
		return {
			execute: function(){
				return $http({
					method: 'POST',
					url: loginURL,
					data: $.param({username: username, password: password}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});
			}
		}
	};
}]);

