var app = angular.module('JustTen', [])

app.factory('submitFormSrvc', ['$http', function($http){
	var submitURL = "http://justten.herokuapp.com/submitPerson";
	return function(
		firstname,
		lastname,
		yob,
		gender,
		email,
		location,
		frumtype,
		soulmateYobStart,
		soulmateYobEnd){
		return {
			execute: function(){
				return $http({
					method: 'POST',
					url: submitURL,
					data: $.param({
						firstname: firstname,
						lastname: lastname,
						yob: yob,
						gender: gender,
						email: email,
						location: location,
						frumtype: frumtype,
						soulmateYobStart: soulmateYobStart,
						soulmateYobEnd: soulmateYobEnd
					}),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				});

			}
		};
	}
}]);

app.controller('justtenCtrl', ['$scope', 'submitFormSrvc', function($scope, submitFormSrvc){
	$scope.status = {};
	$scope.status.unfilledfields = false;
	$scope.status.submitting = false;
	$scope.status.submittedSuccesfully = false;

	$scope.form = {};
	$scope.form.firstname = "";
	$scope.form.lastname = "";
	$scope.form.yob = "";
	$scope.form.gender = "";
	$scope.form.email = "";
	$scope.form.location = "";
	$scope.form.frumtype = {};
	$scope.form.soulmateYobStart = "";
	$scope.form.soulmateYobEnd = "";

	$scope.submitform = function(){
		console.log(
			$scope.form.firstname 	=== '' ||
			$scope.form.lastname 	=== '' ||
			$scope.form.yob 		=== '' ||
			$scope.form.gender 		=== '' ||
			$scope.form.email 		=== '' ||
			$scope.form.location 	=== '' ||
			$scope.form.frumtype 	=== {}
			);	

		if (
			$scope.form.firstname 	=== '' ||
			$scope.form.lastname 	=== '' ||
			$scope.form.yob 		=== '' ||
			$scope.form.gender 		=== '' ||
			$scope.form.email 		=== '' ||
			$scope.form.location 	=== '' ||
			$scope.form.frumtype 	=== {} ||
			$scope.form.soulmateYobStart === '' ||
			$scope.form.soulmateYobEnd === ''
		)
		{
			$scope.status.unfilledfields = true;
			return;
	}
	else{
		$scope.status.unfilledfields = false;
		$scope.status.submitting = true;
	}

		//convert frumtype to an array
		var frumtypeArray = [];
		for (key in $scope.form.frumtype){
			frumtypeArray.push(key);
		}

		submitFormSrvc(
			$scope.form.firstname,
			$scope.form.lastname,
			$scope.form.yob,
			$scope.form.gender,
			$scope.form.email,
			$scope.form.city,
			$scope.form.frumtype,
			$scope.form.soulmateYobStart,
			$scope.form.soulmateYobEnd
			).execute()
		.success(function(data, status){
			console.log(data);
			console.log(status);
			if (status === 200){
				$scope.status.submittedSuccesfully = true;
			}
		})
		.error(function(data, status){
			console.log(data);
			console.log(status);
		});

	};
}])

