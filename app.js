/* 
 * petdating-nodejs
 * 
 * @author Daniele Gazzelloni
 */

// IMPORTS AND CONFIGURATION
// ==============================================

var express     = require("express");
var app         = express();
var session     = require('express-session');
var _           = require('underscore');
var crypto      = require('crypto');
var bodyParser  = require('body-parser');
var multer      = require('multer');
var engines     = require('consolidate');
var moment      = require('moment');
var mongoose    = require("mongoose");
var logger      = require("./logger");
var config      = require("./config");
var connection  = mongoose.connect(config.db_hostname).connection;
var db          = require("./db")(connection);

/*
var allowCrossDomain = function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
};*/
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(express.static(__dirname + '/public'));
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/public');
app.set('view engine', 'html');

app.use(session({secret: 'tententen', name: 'justten', cookie: { maxAge: 86400000 }}))

app.use(allowCrossDomain);

app.use( bodyParser.json() );       // to support JSON-encoded bodies

app.use(bodyParser.urlencoded({     // to support url encoding
    extended: true
}));

app.use(multer({
    dest: config.tempPath,
    inMemory: true
}));


var adminauth = function(req, res, next){
    if (!req.session.admin){
        res.status(403).send("Access Denied");
    }
    else{
        next();
    }
}
// ROUTES
// ==============================================
app.get("/admin", function(req, res){
    res.render('admin/src/index.html');
});
app.get("/user", function(req, res){
    res.sendfile('./public/user/index.html');
});

app.get("/testing", function(req, res){
    res.send("Testing Working");
});

// ADMIN LOGIN AND LOGOUT
app.post("/adminlogin", function(req, res){
    var name    = req.body.username;
    var pass    = req.body.password;
    // Hash password
    var hashedPass = crypto.createHash('md5').update(pass).digest('hex');

    if (name === 'ben' && hashedPass === 'ae59ab9a1a559343e5946fa14cf76ea1'){
        var sess = req.session;
        sess.admin = true;
        res.status(200).send("Login Sucessful");
    }
    else {
        res.status(403).send("Incorrect Info");
    }
});
app.get("/adminlogout", function(req, res){
    req.session.destroy();
    res.status(200).send("Logged Out");
});

// RETRIEVES A LIST OF PERSONS
app.get("/" + config.APIName_getPersons, function (req, res) {
    var sess = req.session;
    if (!sess.admin){
        res.status(403).send("Access Denied");
    }
    else {
        db.getPersons(function(result) {
            res.json( result );
        });
        logger.log("<", "called " + config.APIName_getPersons + ".");
    }
});
// RETRIEVES A SPECIFIC PERSON
app.get('/getPerson/:personId', adminauth, function(req, res){
        db.getPerson(req.params.personId, function(result) {
            res.json(result);
        });
        logger.log("<", "called getPerson.");
});
app.post('/:personId/gotMarried', adminauth, function(req, res){
    db.personGotMarried(req.params.personId, function(result){
        res.json(result);
    });
});
//RETRIVE PEOPLE NAMES GIVEN IDs
app.post("/" + config.APIName_getNames, function (req, res) {
    logger.log(req.body.people);
    var sess = req.session;
    if (!sess.admin){
        res.status(403).send("Access Denied");
    }
    else {
        if (req.body !== undefined) {
            db.getNames(req.body.people, function(result) {
                res.json( {result: result} );
            });
            logger.log("<", "called " + config.APIName_getNames + ".");
        }
    }
});

// RETRIEVES A LIST OF EVENTS AND THEIR DATA
app.get("/" + config.APIName_getEvents, adminauth, function (req, res) {
    db.getEvents(function(result) {
        res.json(result);
    });
    logger.log("<", "called " + config.APIName_getEvents + ".");
});
app.get('/getEvent/:eventId', adminauth, function(req, res){
    db.getEvent(req.params.eventId, function(result){
        res.json(result);
    });
    logger.log("< called getEvent");
});
// SUBMIT A PERSON
app.post("/" + config.APIName_submitPerson, function (req, res) {
    console.log(req.body.firstname + " " + req.body.lastname  + " submitted at " + moment().format('MMMM Do YYYY, h:mm:ss a'));
    if (req.body !== undefined) {
        db.createPerson(req.body.firstname, req.body.lastname, req.body.yob, req.body.gender, req.body.email, req.body.location, req.body.frumtype, req.body.soulmateYobStart, req.body.soulmateYobEnd, req.body.soulmateFrumtype, function(result) {
            res.status(result.code).send( {result: result} );
        });
        logger.log("<", "called " + config.APIName_submitPerson + ".");
    }
});

// DELETE A PERSON
app.post("/" + config.APIName_deletePerson, function (req, res) {
    var sess = req.session;
    if (!sess.admin){
        res.status(403).send("Access Denied");
    }
    else { 
     if (req.body !== undefined) {
        db.deletePerson(req.body.objId, function(result) {
            res.json( {result: result} );
        });
        logger.log("<", "called " + config.APIName_deletePerson + ".");
    }
}
});

// PREPARE A NEW EVENT: OBTAIN WHO THE PEOPLE WILL BE
app.post("/" + config.APIName_prepareEvent, function (req, res) {
    var sess = req.session;
    if (!sess.admin){
        res.status(403).send("Access Denied");
    }
    else {
        if (req.body !== undefined) {
            db.prepareEvent(req.body.size, function(result) {
                res.json( result );
            });
            logger.log("<", "called " + config.APIName_prepareEvent + ".");
        }
    }
});
// CREATE A NEW EVENT: SAVE IT TO DATABASE
app.post("/" + config.APIName_createEvent, adminauth, function (req, res) {
    if (req.body !== undefined) {
        console.log("**CreateEvent men**");
        console.log(req.body.men);
        db.createEvent(req.body.name, req.body.size, req.body.location, req.body.date, req.body.men, req.body.women, req.body.note, function(result) {
            res.json( result );
        });
        logger.log("<", "called " + config.APIName_createEvent + ".");
    }
});
// REMOVE A USER FROM EVENT
app.delete('/event/:eventId/user/:gender/:userId', adminauth, function(req, res){
    db.removeUserFromEvent(req.params.eventId, req.params.gender, req.params.userId, function(result){
        res.json(result);
    });
    logger.log("<", "called " + "removeUserFromEvent" + ".");
});
app.post('/event/:eventId/user/:gender/:userId', adminauth, function(req, res){
    db.addUserToEvent(req.params.eventId, req.params.gender, req.params.userId, function(result){
        res.json(result);
    });
});

app.get("/event/:eventId/generate/:gender", adminauth, function(req, res){
    db.generateGuest(req.params.eventId, req.params.gender, function(result){
        res.json(result);
    });
    logger.log("<", "called " + "generate a new " + req.params.gender + " for event " + req.params.eventId +  ".");
});
// DELETE AN EVENT
app.delete("/deleteEvent", adminauth, function (req, res) {
    if (req.body !== undefined) {
        db.deleteEvent(req.body.id, function(result) {
            res.json( result );
        });
        logger.log("<", "called " + config.APIName_deleteEvent + ".");
    }
});






// STARTING SERVER
// ==============================================
db.connection.on("error", console.error);

db.connection.on("open", function (callback) {
    // Init MongoDB models and schemas
    db.initData(function() {

        // It takes the port from process.env.PORT if available, 
        // OR from config.server_port otherwise.
        var server = app.listen(process.env.PORT || 7071, function () { //7070
            logger.log("*", "listening on port " + server.address().port + ".");
            //TIMESTAMP
            logger.log('**', moment().format('MMMM Do YYYY, h:mm:ss a'), '**');
        });

        logger.log("*", "MongoDB: successfully connected to "+ config.db_hostname+".");
    });
});