/* 
 * Configuration file. 
 * All the editables settings can be found here.
 * 
 * @author Daniele Gazzelloni
 */


// GENERAL
// ==============================================

                                                    
exports.appName             = "justten-nodejs";   // App name (mainly used 
                                                    // for logging purpose).
                                                    
exports.server_port          = 7070;                 // Port at which this module
                                                    // will accept requests.
exports.APIName_getPersons  = "getPersons";
exports.APIName_getNames 	= "getNames";
exports.APIName_getEvents   = "getEvents";
exports.APIName_submitPerson= "submitPerson";
exports.APIName_deletePerson= "deletePerson";
exports.APIName_prepareEvent= "prepareEvent";
exports.APIName_createEvent = "createEvent";
exports.APIName_deleteEvent = "deleteEvent";

exports.gender_male         = "male";               // Male identificator (internal)
exports.gender_female       = "female";             // Female identificator (internal)


// MESSAGES
// ==============================================

// Generated when numberOf(men) < size(newEvent)
exports.messages_noMenFound         = "Try a smaller event." 

// Generated when numberOf(women that matches given men) < size(newEvent)
exports.messages_noWomenFound       = "Not enough matches. Try a smaller event";

exports.code_notEnoughPeople 		= 503;

// Generated when:
//  1) trying to delete a person that is not in the database.
//  2) trying to retrieve persons from the database and the db is empty
//  3) trying to retrieve only men or only women from the database, and 
//     there is no men or no women.
exports.messages_noElementsFound    = "No elements found."

exports.messages_noEventsFound    = "No such events found."

// Generatd as a positive result for many things
exports.messages_OK                 = "OK!";
exports.code_OK                 = 200;



// LOGGING
// ==============================================

exports.verboseLogging       = true;                // Put to false to show only 
                                                    // essential messages in
                                                    // terminal.


// DATABASE
// ==============================================

exports.db_name              	= "justten";  // DB name
exports.db_hostname          	= "mongodb://admin:bennyboy@ds031972.mongolab.com:31972/" + this.db_name;

//exports.db_name              	= "pets";  // DB name
//exports.db_hostname          	= "mongodb://admin:bennyboy@ds031802.mongolab.com:31802/" + this.db_name;



//exports.db_name = "pets"; // DB name
//exports.db_hostname = "mongodb://admin:bennyboy@ds031802.mongolab.com:31802/"    + this.db_name;