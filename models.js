var mongoose        = require('mongoose');
var autoincrement   = require("mongoose-auto-increment");

var models      = {};
var schemas     = {};
// Init schemas (persons and eventS)
schemas.person = new mongoose.Schema({
    firstname: {type: String},
    lastname: {type: String},
    email: {type: String},
    gender: {type: String}, 
    yob: Number,
    location: {type: String},
    frumtype: [{type: String, lowercase: true}],
    soulmateYobStart: Number,
    soulmateYobEnd: Number,
    soulmateFrumtype: [{type: String, lowercase: true}],
    matches: [{type: mongoose.Schema.Types.ObjectId, ref: models.Person}],
    matchesLength: Number,
    married: Boolean,
    eventsAttended: [{type: mongoose.Schema.Types.ObjectId, ref: 'Event'}],
    created: { type: Date, default: Date.now}
});
models.Person = mongoose.model("Person", schemas.person);

schemas.event = new mongoose.Schema({
    eventId: Number,
    name: {type: String},
    size: Number,
    location: {type: String},
    date: {type: Date},     
    men: [{type: mongoose.Schema.Types.ObjectId, ref: models.Person}],
    women: [{type: mongoose.Schema.Types.ObjectId, ref: models.Person}],
    note: {type: String}
});
schemas.event.plugin(autoincrement.plugin, { model: 'Event', field: 'eventId' });
// init models
models.Event = mongoose.model("Event", schemas.event);


module.exports = models;