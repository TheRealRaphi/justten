module.exports = function(grunt) {
  grunt.initConfig({
    jshint: {
      files: ['app.js', 'user/**/*.js'],
      options: {
        globals: {
        }
      }
    },
    watch: {
      files: ['*.js'],
      tasks: ['jshint']
    },

    sass: {
      dist: {
        options: {
        },
        files: {
          'public/admin/src/style/style.css': 'public/admin/src/style/sass/style.scss'
        }
      }
    },

    html2js: {
      options: {
        module: 'ModalsTemplatesApp',
        rename: function(moduleName){
          //take only name of file, without dot
          var i = moduleName.lastIndexOf('/');
          return moduleName.substr(i+1).split('.')[0];
        }
      },
      modals: {
        src: ['public/common/modals/templates/*.tpl.html'],
        dest: 'public/common/modals/dist/modal-templates.js'
      },
    },

    preprocess : {
      prod: {
        src: "C:/Users/Raphi Stein/Projects/Set Study/Backend2/public/common/js/constants.dev.js",
        dest: "C:/Users/Raphi Stein/Projects/Set Study/Backend2/public/common/js/constants.js",
        options: {
          context : {
            URL_BASE: '"https://setstudy.herokuapp.com/api");',
NODE_ENV: 'prod'
}
}
},
dev: {
  src: "C:/Users/Raphi Stein/Projects/Set Study/Backend2/public/common/js/constants.dev.js",
  dest: "C:/Users/Raphi Stein/Projects/Set Study/Backend2/public/common/js/constants.js",
  options: {
    context : {
      URL_BASE: '"http://localhost:7070/api");',
      NODE_ENV: 'dev'
    }
  }
}
},
createModule:{

}
});


grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-watch');
grunt.loadNpmTasks('grunt-contrib-sass');
grunt.loadNpmTasks('grunt-html2js');
grunt.loadNpmTasks('grunt-preprocess');

grunt.registerTask('sasscomp', ['sass']);

grunt.registerTask('createModule', 'Creates an app and controller', function(){
  grunt.file.write(grunt.option('name'));
});

  /*
  grunt.registerTask('fakedata', 'upload fake data', function(){
    var db    = mongoose.connect(config.db_hostname);
    db        = mongoose.connection;
    db.on("open", function(callback){
      console.log("Databse open");
    });
    db.on("error", function(callback){
      console.log("Error");
    })
    console.log("Testing Worked!");
  });
*/

  // WIPE DATABSE AND RESET COUNTERS
  /*
  grunt.registerTask('wipe', 'wipe database and reset counters', function(){

  });
*/
}; 