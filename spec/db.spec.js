var db;			
var config;
var mongoose;	
var models;		
var mocks;



var customMatchers = {
	toContainRef: function(util, customEqualityTesters){
		return {
			compare: function(actual, expected){
				// takes an array of ids, and an id expected to appear in the array
				console.log("toContainRef");
				console.log(actual);
				console.log(expected);

				if (expected===undefined){
					expected='';
				}

				var result = {};

				for (var k = 0; k < actual.length; k++){
					if (new String(actual[k]).valueOf() == new String(expected).valueOf()){
						result.pass 	= true;
						result.message 	= "Array contains a ref to " + expected;
						break;
					}
				}
				if (!result.pass){
					result.message 		= "Array did not contain ref to " + expected;
				}

				return result;

			}
		}
	}
}

describe("DB People Matching", function(){

	jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000; //30 seconds

	var connection;
	beforeAll(function(done){
		jasmine.addMatchers(customMatchers);

		mongoose	= require('mongoose');
		connection 	= mongoose.connect('mongodb://admin:779000@ds039010.mongolab.com:39010/justten-testing').connection;
		db 			= require('../db')(connection);
		config 		= require('../config');
		models		= require('../models');
		personMocks		= require('./person.mocks.spec');

		//console.log(mocks);
		// MOCK USERS

		// Reset the database
		models.Person.remove({}, function(err, result){
			if (err) {
				console.log(err);
			}
			else {
				console.log("	cleaned People collection");	

				models.Event.remove({}, function(err, result){
					if (err) {
						console.log(err);
					}
					else {
						console.log("	cleaned Event collection");

						done();
					}
				});
			}
		});
	});


	beforeEach(function(done){		
		done();
	});


	xit("should insert all the people into the people collection", function(done){
		
		
		personMocks.forEach(function(person, index){
			var person = new models.Person(person);
			person.save(function(err, result){

				if (index === personMocks.length-1){
					done();
				}

			});
		});
	});
	xit("should verify that there are 8 people in the people collection", function(done){
		
		models.Person.find({}, function(err, result){
			expect(result.length).toEqual(personMocks.length);
			done();
		});
	});
	it("should put M1 then F1 and see if F1 is in match for M1", function(done){
		var m1 = personMocks[0];
		var f1 = personMocks[4];

		db.createPerson(m1.firstname, m1.lastname, m1.yob, m1.gender, m1.email, m1.location, m1.frumtype, m1.soulmateYobStart, m1.soulmateYobEnd, m1.soulmateFrumtype, function(){
			db.createPerson(f1.firstname, f1.lastname, f1.yob, f1.gender, f1.email, f1.location, f1.frumtype, f1.soulmateYobStart, f1.soulmateYobEnd, f1.soulmateFrumtype, function(){
				models.Person.findOne({"firstname": "M1"}, function(err, m1){
					if (err) console.log(err);
					else {
						models.Person.findOne({"firstname": "F1"}, function(err, f1){
							if (err) console.log(err);
							else {
								expect(f1.matches).toContainRef(m1._id);
								done();
							}
						});
					}
				});
			});
		});
	});
	it("should put in all the men", function(done){
		var m2	= personMocks[1];
		var m3	= personMocks[2];
		var m4	= personMocks[3];

		db.createPerson(m2.firstname, m2.lastname, m2.yob, m2.gender, m2.email, m2.location, m2.frumtype, m2.soulmateYobStart, m2.soulmateYobEnd, m2.soulmateFrumtype, function(){
			db.createPerson(m3.firstname, m3.lastname, m3.yob, m3.gender, m3.email, m3.location, m3.frumtype, m3.soulmateYobStart, m3.soulmateYobEnd, m3.soulmateFrumtype, function(){
				db.createPerson(m4.firstname, m4.lastname, m4.yob, m4.gender, m4.email, m4.location, m4.frumtype, m4.soulmateYobStart, m4.soulmateYobEnd, m4.soulmateFrumtype, function(code){
					expect(code).toEqual(config.code_OK);
					done();
				});
			});
		});
	});
	it("should check that database contains 5 people", function(done){
		models.Person.find({}, function(err, result){
			if (err) console.log(err);
			else {
				expect(result.length).toBe(5);
				done();
			}
		});
	});
	it("should put in all the women", function(done){
		var f2	= personMocks[5];
		var f3	= personMocks[6];
		var f4	= personMocks[7];

		db.createPerson(f2.firstname, f2.lastname, f2.yob, f2.gender, f2.email, f2.location, f2.frumtype, f2.soulmateYobStart, f2.soulmateYobEnd, f2.soulmateFrumtype, function(){
			db.createPerson(f3.firstname, f3.lastname, f3.yob, f3.gender, f3.email, f3.location, f3.frumtype, f3.soulmateYobStart, f3.soulmateYobEnd, f3.soulmateFrumtype, function(){
				db.createPerson(f4.firstname, f4.lastname, f4.yob, f4.gender, f4.email, f4.location, f4.frumtype, f4.soulmateYobStart, f4.soulmateYobEnd, f4.soulmateFrumtype, function(code){
					expect(code).toEqual(config.code_OK);
					done();
				});
			});
		});
	});
	it("should check that database contains 8 people", function(done){
		models.Person.find({}, function(err, result){
			if (err) console.log(err);
			else {
				expect(result.length).toBe(8);
				done();
			}
		});
	});
	it("it should generate 10 random numbers and be of length 10", function(done){
		var arr = db.hidden.pickXRandomNumbersInRange(5, 5);
		expect(arr.length).toBe(5);
		done();
	});
	it("it should generate x random numbers between 0 and y", function(done){
		var arr = db.hidden.pickXRandomNumbersInRange(5, 5);
		expect((arr[0] < 6) && 
			(arr[1] < 6) &&
			(arr[2] < 6) &&
			(arr[3] < 6) &&
			(arr[4] < 6)).toBeTruthy();
		done();
	});
	it("should prepare an event with 2 people which must include two men", function(done){
		db.prepareEvent(2, function(event){
			expect(event.men.length).toBe(2);
			done();
		});
	});
	it("should prepare an event with 2 people which must include two women", function(done){
		db.prepareEvent(2, function(event){
			expect(event.women.length).toBe(2);
			done();
		});
	});

	xit("should return most popular men aka M2 and M1", function(done){
		db.hidden.getPopularMen(2, function(popularMen){
			console.log("popularMen");
			console.log(popularMen);
			done();
		});
	});
	it("should prepare and create an event", function(done){
		db.prepareEvent(2, function(event){
			db.createEvent("Test Event 1", 2, "TestLocation", "07/10/2015", event.men, event.women, "Test Event 1", function(createEventResult){
				models.Person.findOne({"firstname": personMocks[0].firstname}, function(err, m1){
					if (err) console.log(err);
					else {
						expect(createEventResult.event.name).toEqual("Test Event 1");
						done();
					}
				});
			});
		});
	});
	it("should verify that a man in event array is matched with woman in the women array", function(done){
		models.Event.findOne({"name": "Test Event 1"}, function(err, event){
			models.Person.findOne({"_id": event.men[0]}, function(err, man){
				// get this man's matches, and make sure women from this event are in his matches
				// console.log("man matches");
				// console.log(man.matches);
				// console.log("event.women");
				// console.log(event.women);
				expect(man.matches.indexOf(event.women[0]) > -1).toBeTruthy();
				done();
			});
		});
	});
	it("should verify that M2 is in the event men array", function(done){
		models.Event.findOne({"name": "Test Event 1"}, function(err, event){
			models.Person.findOne({"firstname": personMocks[1].firstname}, function(err, m2){
				if (err) console.log(err);
				else {
					expect(event.men).toContainRef(m2._id);
					done();
				}
			});
		});
	});
	it("should verify that event has been added to person's event array", function(done){
		models.Event.findOne({"name": "Test Event 1"}, function(err, event){
			models.Person.findOne({"_id": event.men[0]}, function(err, man){
				expect(man.eventsAttended.indexOf(event._id)).toBeGreaterThan(-1);
				done();
			});
		});
	});
	it("should get a person with his eventsAttended array populated", function(done){
		models.Event.findOne({"name": "Test Event 1"}, function(err, event){
			// take some man in this array
			models.Person.findOne({"_id": event.men[0]}).populate('eventsAttended').exec(function(err, man){
				expect(man.eventsAttended[0].name).toEqual('Test Event 1');
				done();
			});


		});
	});
	afterAll(function(done){
		//mongoose.connection.close();

		connection.close(function(){
			console.log("Connection Closed");
			done();
		});
	});
});

